package angelectro.com.schoolandroid.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import angelectro.com.schoolandroid.R;
import angelectro.com.schoolandroid.content.current.CurrentWeather;
import angelectro.com.schoolandroid.database.WeatherTable;
import angelectro.com.schoolandroid.dialog.LoadingDialog;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zahit Talipov on 23.06.2016.
 */
public class CurrentWeatherFragment extends Fragment {
    public static String CITY_ID = "cityId";
    @BindView(R.id.icon_image)
    ImageView mImageViewIcon;
    @BindView(R.id.humidity)
    TextView mTextViewHumidity;
    @BindView(R.id.pressure)
    TextView mTextViewPressure;
    @BindView(R.id.description)
    TextView mTextViewDescription;
    @BindView(R.id.wind)
    TextView mTextViewWind;
    @BindView(R.id.temp)
    TextView mTextViewTemp;


    public static CurrentWeatherFragment create(String id) {
        Bundle bundle = new Bundle();
        bundle.putString(CITY_ID, id);
        CurrentWeatherFragment currentWeatherFragment = new CurrentWeatherFragment();
        currentWeatherFragment.setArguments(bundle);
        return currentWeatherFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.current_weather_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        init();
    }

    public void init() {
        String cityId = getArguments().getString(CITY_ID);
        CurrentWeather currentWeather = WeatherTable.getCurrentWeatherById(getContext(), cityId);
        getActivity().setTitle(currentWeather.getName());
        Picasso.with(getContext())
                .load("http://openweathermap.org/img/w/" + currentWeather.getWeather().get(0).getIcon() + ".png")
        .into(mImageViewIcon);
        mTextViewDescription.setText(currentWeather.getWeather().get(0).getDescription());
        mTextViewHumidity.setText(currentWeather.getMain().getHumidity()+ " %");
        mTextViewPressure.setText(currentWeather.getMain().getPressure()+ " hpa");
        mTextViewTemp.setText(String.valueOf(currentWeather.getMain().getTemp().intValue())+ "°");
        mTextViewWind.setText(currentWeather.getWind().getSpeed()+" m/s");
    }
}
