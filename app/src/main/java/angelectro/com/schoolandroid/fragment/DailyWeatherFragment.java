package angelectro.com.schoolandroid.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import angelectro.com.schoolandroid.R;
import angelectro.com.schoolandroid.content.daily.ListDays;
import angelectro.com.schoolandroid.presenter.DailyWeatherPresenter;
import angelectro.com.schoolandroid.view.DailyWeatherView;
import angelectro.com.schoolandroid.widgets.DailyRecyclerAdapter;
import angelectro.com.schoolandroid.widgets.RefreshListener;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zahit Talipov on 24.06.2016.
 */
public class DailyWeatherFragment extends Fragment implements DailyWeatherView {
    public static String CITY_ID = "cityId";
    @BindView(R.id.daily_recycler_view)
    RecyclerView mDailyRecyclerView;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.progress)
    ProgressBar mProgressBar;
    DailyRecyclerAdapter mAdapter;
    DailyWeatherPresenter mPresenter;
    String sCityId;

    public static DailyWeatherFragment create(String id) {
        Bundle bundle = new Bundle();
        bundle.putString(CITY_ID, id);
        DailyWeatherFragment dailyWeatherFragment = new DailyWeatherFragment();
        dailyWeatherFragment.setArguments(bundle);
        return dailyWeatherFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.daily_weather_fragment, container, false);
        ButterKnife.bind(this,view);
        sCityId = getArguments().getString(CITY_ID);
        mPresenter = new DailyWeatherPresenter(getContext(), this, sCityId);
        mDailyRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new DailyRecyclerAdapter(getContext());
        mDailyRecyclerView.setAdapter(mAdapter);
        mRefreshLayout.setOnRefreshListener(new RefreshListener(mPresenter));
        mPresenter.refresh();
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void showResult(List<ListDays> listDays) {
        mAdapter.update(listDays);
    }

    @Override
    public void showError(Throwable throwable) {
        mRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showLoading() {
        mRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        mRefreshLayout.setRefreshing(false);
        mProgressBar.setVisibility(View.GONE);
    }
}
