package angelectro.com.schoolandroid.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import angelectro.com.schoolandroid.R;
import angelectro.com.schoolandroid.content.Day;
import angelectro.com.schoolandroid.presenter.ForecastWeatherPresenter;
import angelectro.com.schoolandroid.view.ForecastWeatherView;
import angelectro.com.schoolandroid.widgets.ForecastRecyclerAdapter;
import angelectro.com.schoolandroid.widgets.RefreshListener;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zahit Talipov on 24.06.2016.
 */
public class ForecastWeatherFragment extends Fragment implements ForecastWeatherView {

    private static final String CITY_ID = "city_id";
    @BindView(R.id.recycler_view_forecast_weather)
    RecyclerView mRecyclerView;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.progress)
    ProgressBar mProgressBar;
    ForecastRecyclerAdapter mAdapter;
    ForecastWeatherPresenter mPresenter;
    private String sCityId;

    public static ForecastWeatherFragment create(String id) {
        Bundle bundle = new Bundle();
        bundle.putString(CITY_ID, id);
        ForecastWeatherFragment forecastWeatherFragment = new ForecastWeatherFragment();
        forecastWeatherFragment.setArguments(bundle);
        return forecastWeatherFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.forecast_weather_fragment, container, false);
        ButterKnife.bind(this, view);
        sCityId = getArguments().getString(CITY_ID);
        mPresenter = new ForecastWeatherPresenter(getContext(), this, sCityId);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new ForecastRecyclerAdapter();
        mRecyclerView.setAdapter(mAdapter);
        mRefreshLayout.setOnRefreshListener(new RefreshListener(mPresenter));
        mPresenter.refresh();
        return view;
    }

    @Override
    public void showResult(List<Day> listDates) {
        mAdapter.update(listDates);
    }

    @Override
    public void showError(Throwable throwable) {
        mRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showLoading() {
        mRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        mRefreshLayout.setRefreshing(false);
        mProgressBar.setVisibility(View.GONE);
    }
}
