package angelectro.com.schoolandroid.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import angelectro.com.schoolandroid.R;
import angelectro.com.schoolandroid.content.daily.DailyWeather;
import angelectro.com.schoolandroid.fragment.CurrentWeatherFragment;
import angelectro.com.schoolandroid.fragment.DailyWeatherFragment;
import angelectro.com.schoolandroid.fragment.ForecastWeatherFragment;

/**
 * Created by Zahit Talipov on 23.06.2016.
 */
public class WeatherPagerAdapter extends FragmentPagerAdapter {
    Context mContext;
    String[] mTitles;
    String mCityId;
    public WeatherPagerAdapter(@NonNull FragmentManager fm, @NonNull Context context, @NonNull String id) {
        super(fm);
        mContext = context;
        mTitles = context.getResources().getStringArray(R.array.tabHeaders);
        mCityId =id;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:{
                return CurrentWeatherFragment.create(mCityId);
            }
            case 1:{
                return ForecastWeatherFragment.create(mCityId);
            }
            case 2:{
                return DailyWeatherFragment.create(mCityId);
            }
        }
        return null;
    }

    @Override
    public int getCount() {
        return mTitles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles[position];
    }
}
