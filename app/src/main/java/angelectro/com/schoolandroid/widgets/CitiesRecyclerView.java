package angelectro.com.schoolandroid.widgets;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by Zahit Talipov on 22.06.2016.
 */
public class CitiesRecyclerView extends RecyclerView {

    private CurrentWeatherRecyclerAdapter mAdapter;

    public CitiesRecyclerView(Context context) {
        super(context);
        init();
    }

    public CitiesRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CitiesRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    @Override
    public CurrentWeatherRecyclerAdapter getAdapter() {
        return mAdapter;
    }

    public void init() {
        this.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new CurrentWeatherRecyclerAdapter();
        this.setAdapter(mAdapter);
    }


}
