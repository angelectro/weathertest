package angelectro.com.schoolandroid.widgets;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import angelectro.com.schoolandroid.BuildConfig;
import angelectro.com.schoolandroid.FormatterTools;
import angelectro.com.schoolandroid.R;
import angelectro.com.schoolandroid.content.Day;
import angelectro.com.schoolandroid.content.forecast.ListDate;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zahit Talipov on 24.06.2016.
 */
public class ForecastRecyclerAdapter extends RecyclerView.Adapter<ForecastRecyclerAdapter.Holder> {

    List<Day> mDays = new ArrayList<>();
    Context mContext;

    public void update(List<Day> days) {
        mDays = days;
        notifyDataSetChanged();
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_forecat, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Day day = mDays.get(position);
        holder.mViewDate.setText(day.getDate());
        holder.mViewRoot.removeAllViews();
        for (ListDate date : day.getListDates()) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_hour_farecast, null);
            HolderHour holderHour = new HolderHour(view);
            holderHour.mTextViewHour.setText(FormatterTools.getHourString(date.getDt_txt()));
            holderHour.mTextViewHumidity.setText(date.getMain().getHumidity() + " %");
            holderHour.mTextViewPressure.setText(date.getMain().getPressure() + " hpa");
            holderHour.mTextViewTemp.setText(date.getMain().getTemp().intValue() + "°C");
            if (date.getWind()!= null)
                holderHour.mTextViewWind.setText(date.getWind().getSpeed() + " m/s");
            Picasso.with(mContext)
                    .load(BuildConfig.API_ENDPOINT_ICON + date.getWeather().get(0).getIcon() + ".png")
                    .into(holderHour.mImageViewIcon);
            holder.mViewRoot.addView(view);
        }

    }

    @Override
    public int getItemCount() {
        return mDays.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.date)
        TextView mViewDate;
        @BindView(R.id.root)
        LinearLayout mViewRoot;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class HolderHour {
        @BindView(R.id.icon_image)
        ImageView mImageViewIcon;
        @BindView(R.id.humidity)
        TextView mTextViewHumidity;
        @BindView(R.id.pressure)
        TextView mTextViewPressure;
        @BindView(R.id.wind)
        TextView mTextViewWind;
        @BindView(R.id.temp)
        TextView mTextViewTemp;
        @BindView(R.id.hour)
        TextView mTextViewHour;

        public HolderHour(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
