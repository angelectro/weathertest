package angelectro.com.schoolandroid.widgets;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import angelectro.com.schoolandroid.BuildConfig;
import angelectro.com.schoolandroid.FormatterTools;
import angelectro.com.schoolandroid.R;
import angelectro.com.schoolandroid.content.daily.ListDays;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zahit Talipov on 24.06.2016.
 */
public class DailyRecyclerAdapter extends RecyclerView.Adapter<DailyRecyclerAdapter.Holder> {

    List<ListDays> mListDays = new ArrayList<>();
    Context mContext;

    public DailyRecyclerAdapter(Context context) {
        mContext = context;
    }

    public void update(List<ListDays> list) {
        mListDays = list;
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_daily, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        ListDays day = mListDays.get(position);
        Picasso.with(mContext)
                .load(BuildConfig.API_ENDPOINT_ICON + day.getWeather().get(0).getIcon() + ".png")
                .into(holder.mImageViewIcon);
        holder.mTextViewClouds.setText(day.getClouds()+" %, "+ day.getPressure()+" hpa");
        holder.mTextViewTempDay.setText(day.getTemp().getDay().intValue() + " °C");
        holder.mTextViewTempNight.setText(day.getTemp().getNight().intValue() + " °C");
        holder.mTextViewWind.setText(day.getSpeed() + " m/s");
        holder.mTextViewDescription.setText(day.getWeather().get(0).getDescription());
        holder.mTextViewDate.setText(FormatterTools.getDateString(day.getDt()));
    }

    @Override
    public int getItemCount() {
        return mListDays.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.icon_image)
        ImageView mImageViewIcon;
        @BindView(R.id.temp_night)
        TextView mTextViewTempNight;
        @BindView(R.id.temp_day)
        TextView mTextViewTempDay;
        @BindView(R.id.wind)
        TextView mTextViewWind;
        @BindView(R.id.description)
        TextView mTextViewDescription;
        @BindView(R.id.clouds_and_pressure)
        TextView mTextViewClouds;
        @BindView(R.id.date)
        TextView mTextViewDate;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
