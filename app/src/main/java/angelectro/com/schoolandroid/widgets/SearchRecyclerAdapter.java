package angelectro.com.schoolandroid.widgets;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import angelectro.com.schoolandroid.R;
import angelectro.com.schoolandroid.content.find.ListCities;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zahit Talipov on 24.06.2016.
 */
public class SearchRecyclerAdapter extends RecyclerView.Adapter<SearchRecyclerAdapter.Holder> {

    List<ListCities> mListCities = new ArrayList<>();
    OnItemClickListener mListener;

    public SearchRecyclerAdapter(OnItemClickListener listener) {
        mListener = listener;
    }

    public void update(List<ListCities> listCities) {
        mListCities = listCities;
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.mTextViewName.setText(mListCities.get(position).getCityName());
    }

    @Override
    public int getItemCount() {
        return mListCities.size();
    }

    public interface OnItemClickListener {
        public void onClickItem(int id);
    }

    class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.city_name)
        TextView mTextViewName;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setClickable(true);
            itemView.setOnClickListener(v -> mListener.onClickItem(mListCities.get(getLayoutPosition()).getId()));
        }
    }
}
