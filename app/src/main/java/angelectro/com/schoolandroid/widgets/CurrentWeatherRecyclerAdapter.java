package angelectro.com.schoolandroid.widgets;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import angelectro.com.schoolandroid.BuildConfig;
import angelectro.com.schoolandroid.R;
import angelectro.com.schoolandroid.Settings;
import angelectro.com.schoolandroid.content.current.CurrentWeather;
import angelectro.com.schoolandroid.database.WeatherTable;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zahit Talipov on 23.06.2016.
 */
public class CurrentWeatherRecyclerAdapter extends RecyclerView.Adapter<CurrentWeatherRecyclerAdapter.Holder> implements View.OnCreateContextMenuListener {

    Context mContext;
    OnItemClickListener mOnItemClickListener;
    List<CurrentWeather> mCurrentWeathers = new ArrayList<>();
    MenuItem.OnMenuItemClickListener mMenuItemClickListener;

    public void setOnListeners(OnItemClickListener onItemClickListener,
                              MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        mOnItemClickListener = onItemClickListener;
        mMenuItemClickListener = onMenuItemClickListener;
    }

    public List<CurrentWeather> getCurrentWeathers() {
        return mCurrentWeathers;
    }

    public void update(List<CurrentWeather> currentWeathers) {
        mCurrentWeathers = currentWeathers;
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cites, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        CurrentWeather currentWeather = mCurrentWeathers.get(position);
        holder.mView.setId(currentWeather.getId());
        holder.mTextViewCityName.setText(currentWeather.getName());
        holder.mTextViewTemp.setText(String.valueOf(currentWeather.getMain().getTemp().intValue()) + " °C");
        Picasso.with(mContext).load(BuildConfig.API_ENDPOINT_ICON + currentWeather.getWeather().get(0).getIcon() + ".png")
                .into(holder.mImageViewIcon);
    }

    @Override
    public int getItemCount() {
        return mCurrentWeathers.size();
    }

    @Override
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        contextMenu.add(view.getId(), view.getId(), 0, "Delete").setOnMenuItemClickListener(mMenuItemClickListener);

    }

    public interface OnItemClickListener {
        public void onClickItem(int id);
    }

    class Holder extends RecyclerView.ViewHolder {
        View mView;
        @BindView(R.id.city_name)
        TextView mTextViewCityName;
        @BindView(R.id.temp_in_city)
        TextView mTextViewTemp;
        @BindView(R.id.my_weather_icon)
        ImageView mImageViewIcon;

        public Holder(View itemView) {
            super(itemView);
            mView = itemView;
            ButterKnife.bind(this, itemView);
            itemView.setClickable(true);
            itemView.setOnCreateContextMenuListener(CurrentWeatherRecyclerAdapter.this);
            itemView.setOnClickListener(l -> mOnItemClickListener.onClickItem(mCurrentWeathers.get(getLayoutPosition()).getId()));
        }
    }
}
