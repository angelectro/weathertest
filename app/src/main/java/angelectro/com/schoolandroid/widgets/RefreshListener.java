package angelectro.com.schoolandroid.widgets;

import android.support.v4.widget.SwipeRefreshLayout;

import angelectro.com.schoolandroid.presenter.RefreshPresenter;

/**
 * Created by Zahit Talipov on 23.06.2016.
 */
public class RefreshListener implements SwipeRefreshLayout.OnRefreshListener {

    RefreshPresenter mPresenter;

    public RefreshListener(RefreshPresenter refreshPresenter) {
        mPresenter = refreshPresenter;
    }

    @Override
    public void onRefresh() {
        mPresenter.refresh();
    }
}
