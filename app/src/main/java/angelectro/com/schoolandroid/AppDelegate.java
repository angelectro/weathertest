package angelectro.com.schoolandroid;

import android.app.Application;

import angelectro.com.schoolandroid.api.ApiProvider;

/**
 * Created by Zahit Talipov on 22.06.2016.
 */
public class AppDelegate extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ApiProvider.setContext(this);
        Settings.init(this);
        Settings.addCity("551487"); // Kazan
        Settings.addCity("524901"); // Moscow
    }
}
