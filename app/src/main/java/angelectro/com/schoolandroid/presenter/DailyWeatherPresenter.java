package angelectro.com.schoolandroid.presenter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import angelectro.com.schoolandroid.api.ApiProvider;
import angelectro.com.schoolandroid.content.daily.DailyWeather;
import angelectro.com.schoolandroid.database.WeatherTable;
import angelectro.com.schoolandroid.view.DailyWeatherView;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Zahit Talipov on 24.06.2016.
 */
public class DailyWeatherPresenter implements RefreshPresenter{

    Context mContext;
    DailyWeatherView mDailyWeatherView;
    Activity mActivity;
    String mCityId;

    public DailyWeatherPresenter(Context context, DailyWeatherView dailyWeatherView, String id) {
        mContext = context;
        mDailyWeatherView = dailyWeatherView;
        mCityId = id;
    }

    public void refresh() {
        mDailyWeatherView.showLoading();
        ApiProvider.getWeatherService().getDailyWeatherById(mCityId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<DailyWeather>() {
                    @Override
                    public void onCompleted() {
                        mDailyWeatherView.hideLoading();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("weatherDaily", e.getMessage());
                        mDailyWeatherView.showError(e);
                        mDailyWeatherView.hideLoading();
                        show();
                    }

                    @Override
                    public void onNext(DailyWeather dailyWeather) {
                        Log.d("weatherDaily", "save");
                        WeatherTable.saveDaily(mContext, dailyWeather);
                        show();
                    }
                });
    }

    public void show() {
        DailyWeather dailyWeather = WeatherTable.getDailyWeatherById(mContext, mCityId);
        if (dailyWeather != null)
            mDailyWeatherView.showResult(dailyWeather.getList());
    }
}
