package angelectro.com.schoolandroid.presenter;

/**
 * Created by Zahit Talipov on 24.06.2016.
 */
public interface RefreshPresenter {
    public void refresh();
}
