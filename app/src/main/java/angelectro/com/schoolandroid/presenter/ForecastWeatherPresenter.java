package angelectro.com.schoolandroid.presenter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import angelectro.com.schoolandroid.FormatterTools;
import angelectro.com.schoolandroid.api.ApiProvider;
import angelectro.com.schoolandroid.content.forecast.ForecastWeather;
import angelectro.com.schoolandroid.database.WeatherTable;
import angelectro.com.schoolandroid.view.ForecastWeatherView;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Zahit Talipov on 24.06.2016.
 */
public class ForecastWeatherPresenter implements RefreshPresenter{


    Context mContext;
    ForecastWeatherView mForecastWeatherView;
    Activity mActivity;
    String mCityId;

    public ForecastWeatherPresenter(Context context, ForecastWeatherView forecastWeather, String id) {
        mContext = context;
        mForecastWeatherView = forecastWeather;
        mCityId = id;
    }

    public void refresh() {
        mForecastWeatherView.showLoading();
        ApiProvider.getWeatherService().getForecastWeatherById(mCityId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ForecastWeather>() {
                    @Override
                    public void onCompleted() {
                        mForecastWeatherView.hideLoading();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("weatherDaily", e.getMessage());
                        mForecastWeatherView.showError(e);
                        mForecastWeatherView.hideLoading();
                        show();
                    }

                    @Override
                    public void onNext(ForecastWeather forecastWeather) {
                        Log.d("weatherDaily", "save");
                        WeatherTable.saveForecast(mContext, forecastWeather);
                        show();
                    }
                });
    }

    public void show() {
        ForecastWeather forecastWeather = WeatherTable.getForecastWeatherById(mContext, mCityId);
        if (forecastWeather != null)
            mForecastWeatherView.showResult(FormatterTools.convertDay(forecastWeather.getList()));
    }
}
