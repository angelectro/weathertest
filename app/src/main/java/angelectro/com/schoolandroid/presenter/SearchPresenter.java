package angelectro.com.schoolandroid.presenter;

import angelectro.com.schoolandroid.api.ApiProvider;
import angelectro.com.schoolandroid.content.find.FindWeather;
import angelectro.com.schoolandroid.view.SearchCityView;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Zahit Talipov on 24.06.2016.
 */
public class SearchPresenter {

    SearchCityView mSearchCityView;

    public SearchPresenter(SearchCityView searchCityView) {
        mSearchCityView = searchCityView;
    }

    public void find(String city) {
        ApiProvider.getWeatherService()
                .searchCity(city)
                .doOnSubscribe(mSearchCityView::showLoading)
                .doOnTerminate(mSearchCityView::hideLoading)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<FindWeather>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                       mSearchCityView.showError(e);
                    }

                    @Override
                    public void onNext(FindWeather findWeather) {
                         mSearchCityView.showResult(findWeather.getList());
                    }
                });
    }
}
