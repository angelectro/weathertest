package angelectro.com.schoolandroid.presenter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import java.util.List;

import angelectro.com.schoolandroid.Settings;
import angelectro.com.schoolandroid.api.ApiProvider;
import angelectro.com.schoolandroid.content.current.CurrentWeather;
import angelectro.com.schoolandroid.view.MainView;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static angelectro.com.schoolandroid.database.WeatherTable.listOfCitiesWeather;
import static angelectro.com.schoolandroid.database.WeatherTable.saveCurrent;

/**
 * Created by Zahit Talipov on 23.06.2016.
 */
public class MainPresenter implements RefreshPresenter{
    Context mContext;
    MainView mMainView;
    Activity mActivity;

    public MainPresenter(Activity activity) {
        mMainView = (MainView) activity;
        mContext = activity;
        mActivity = activity;
    }

    public void refresh() {
        mMainView.showLoading();
        List<String> listIdCities = Settings.getListIdCities();
        Observable.from(listIdCities)
                .flatMap(ApiProvider.getWeatherService()::getCurrentWeatherById)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CurrentWeather>() {
                    @Override
                    public void onCompleted() {
                        show();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("weatherCurrent", e.getMessage());
                        mMainView.showError(e);
                        show();
                    }

                    @Override
                    public void onNext(CurrentWeather currentWeather) {
                        saveCurrent(mContext, currentWeather);
                    }
                });
    }

    public void show() {
        mMainView.showResult(listOfCitiesWeather(mContext));
    }
}
