package angelectro.com.schoolandroid.view;

import java.util.List;

import angelectro.com.schoolandroid.content.current.CurrentWeather;
import angelectro.com.schoolandroid.content.find.ListCities;

/**
 * Created by Zahit Talipov on 24.06.2016.
 */
public interface SearchCityView {
    public void showResult(List<ListCities> listCities);
    public void showError(Throwable throwable);
    public void showLoading();
    public void hideLoading();
}
