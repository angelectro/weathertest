package angelectro.com.schoolandroid.view;

import java.util.List;

import angelectro.com.schoolandroid.content.daily.ListDays;

/**
 * Created by Zahit Talipov on 24.06.2016.
 */
public interface DailyWeatherView {
    public void showResult(List<ListDays> currentWeathers);

    public void showError(Throwable throwable);

    public void showLoading();

    public void hideLoading();
}
