package angelectro.com.schoolandroid.view;

import java.util.List;

import angelectro.com.schoolandroid.content.Day;
import angelectro.com.schoolandroid.content.forecast.ListDate;

/**
 * Created by Zahit Talipov on 24.06.2016.
 */
public interface ForecastWeatherView {
    public void showResult(List<Day> listDates);

    public void showError(Throwable throwable);

    public void showLoading();

    public void hideLoading();
}
