package angelectro.com.schoolandroid.view;

import java.util.List;

import angelectro.com.schoolandroid.content.current.CurrentWeather;

/**
 * Created by Zahit Talipov on 23.06.2016.
 */
public interface MainView {
    public void showResult(List<CurrentWeather> currentWeathers);
    public void showError(Throwable throwable);
    public void showLoading();
    public void hideLoading();
}
