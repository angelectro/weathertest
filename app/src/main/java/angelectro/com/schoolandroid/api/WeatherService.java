package angelectro.com.schoolandroid.api;

import angelectro.com.schoolandroid.content.current.CurrentWeather;
import angelectro.com.schoolandroid.content.daily.DailyWeather;
import angelectro.com.schoolandroid.content.find.FindWeather;
import angelectro.com.schoolandroid.content.forecast.ForecastWeather;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Zahit Talipov on 22.06.2016.
 */
public interface WeatherService {

    @GET("data/2.5/weather")
    public Observable<CurrentWeather> getCurrentWeather(@Query("q")String city);

    @GET("data/2.5/forecast/daily")
    public Observable<DailyWeather> getDailyWeather(@Query("q")String city);

    @GET("data/2.5/forecast")
    public Observable<ForecastWeather> getForecastWeather(@Query("q")String city);

    @GET("data/2.5/weather")
    public Observable<CurrentWeather> getCurrentWeatherById(@Query("id")String id);

    @GET("data/2.5/forecast/daily")
    public Observable<DailyWeather> getDailyWeatherById(@Query("id")String id);

    @GET("data/2.5/forecast")
    public Observable<ForecastWeather> getForecastWeatherById(@Query("id")String city);

    @GET("data/2.5/find")
    public Observable<FindWeather> searchCity(@Query("q")String city);
}
