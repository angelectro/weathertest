package angelectro.com.schoolandroid.api;

import android.support.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Created by Zahit Talipov on 22.06.2016.
 */
public class OkHttp {
    private static final int CONNECT_TIMEOUT = 15;
    private static final int READ_TIMEOUT = 30;
    private static final int WRITE_TIMEOUT = 30;
    static OkHttpClient mClient = null;

    public static OkHttpClient getClient(@NonNull String apiKey) {
        if (mClient == null) {
            mClient = new OkHttpClient.Builder()
                    .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                    .readTimeout(READ_TIMEOUT,TimeUnit.SECONDS)
                    .writeTimeout(WRITE_TIMEOUT,TimeUnit.SECONDS)
                    .addInterceptor(new ApiKeyInterceptor(apiKey))
                    .build();
        }
        return mClient;
    }
}
