package angelectro.com.schoolandroid.api;

import android.content.Context;

import angelectro.com.schoolandroid.BuildConfig;
import angelectro.com.schoolandroid.R;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Zahit Talipov on 22.06.2016.
 */
public class ApiProvider {

    private static Context mContext;

    public static void setContext(Context mContext) {
        ApiProvider.mContext = mContext;
    }

    public static WeatherService getWeatherService() {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.API_ENDPOINT)
                .client(OkHttp.getClient(mContext.getString(R.string.api_key)))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(WeatherService.class);
    }
}
