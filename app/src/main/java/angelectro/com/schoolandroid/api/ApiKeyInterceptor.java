package angelectro.com.schoolandroid.api;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Zahit Talipov on 22.06.2016.
 */
public class ApiKeyInterceptor implements Interceptor {

    String mApiKey = null;

    public ApiKeyInterceptor(@NonNull String apiKey) {
        mApiKey = apiKey;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        if (TextUtils.isEmpty(mApiKey)) {
            return chain.proceed(chain.request());
        }
        Request request = chain.request();
        HttpUrl httpUrl = request
                .url()
                .newBuilder()
                .addQueryParameter("APPID", mApiKey)
                .addQueryParameter("units","metric").build();
        request = request.newBuilder().url(httpUrl).build();
        return chain.proceed(request);
    }
}
