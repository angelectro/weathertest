package angelectro.com.schoolandroid.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import java.util.List;

import angelectro.com.schoolandroid.R;
import angelectro.com.schoolandroid.Settings;
import angelectro.com.schoolandroid.content.current.CurrentWeather;
import angelectro.com.schoolandroid.database.WeatherTable;
import angelectro.com.schoolandroid.dialog.LoadingDialog;
import angelectro.com.schoolandroid.presenter.MainPresenter;
import angelectro.com.schoolandroid.view.MainView;
import angelectro.com.schoolandroid.widgets.CitiesRecyclerView;
import angelectro.com.schoolandroid.widgets.CurrentWeatherRecyclerAdapter;
import angelectro.com.schoolandroid.widgets.RefreshListener;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainView, CurrentWeatherRecyclerAdapter.OnItemClickListener, MenuItem.OnMenuItemClickListener {
    public static final int REQUEST_CODE = 23;
    @BindView(R.id.recycler_view_current_weather)
    CitiesRecyclerView mCitiesRecyclerView;
    @BindView(R.id.refresh_current)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.fab)
    FloatingActionButton mActionButton;
    MainPresenter mMainPresenter;
    LoadingDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_of_sities);
        ButterKnife.bind(this);
        mMainPresenter = new MainPresenter(this);
        mSwipeRefreshLayout.setOnRefreshListener(new RefreshListener(mMainPresenter));
        mCitiesRecyclerView.getAdapter().setOnListeners(this, this);
        mActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(MainActivity.this, FindCityActivity.class), REQUEST_CODE);
                ;
            }
        });
        mMainPresenter.refresh();
    }

    @Override
    public void showResult(List<CurrentWeather> currentWeathers) {
        mCitiesRecyclerView.getAdapter().update(currentWeathers);
        hideLoading();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            int id_city = data.getIntExtra(FindCityActivity.CITY_ID, 0);
            if (id_city != 0) {
                Settings.addCity(String.valueOf(id_city));
                mMainPresenter.refresh();
            }
        }
    }

    @Override
    public void showError(Throwable throwable) {
        Snackbar.make(mCitiesRecyclerView, R.string.no_connection_error, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showLoading() {
        mDialog = new LoadingDialog();
        mDialog.show(getFragmentManager());
    }

    @Override
    public void hideLoading() {
        mSwipeRefreshLayout.setRefreshing(false);
        if (mDialog != null)
            mDialog.dismiss();
    }

    @Override
    public void onClickItem(int id) {
        Log.d("item", "click");
        DetailedWeatherActivity.create(this, id);
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        int id = menuItem.getItemId();
        Settings.deleteCity(String.valueOf(id));
        WeatherTable.delete(this, String.valueOf(id));
        mMainPresenter.refresh();
        return true;
    }
}
