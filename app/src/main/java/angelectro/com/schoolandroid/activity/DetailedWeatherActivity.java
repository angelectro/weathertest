package angelectro.com.schoolandroid.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;


import angelectro.com.schoolandroid.R;
import angelectro.com.schoolandroid.adapter.WeatherPagerAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zahit Talipov on 23.06.2016.
 */
public class DetailedWeatherActivity extends AppCompatActivity {
    public static String CITY_ID = "mCityId";
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tabLayout)
    TabLayout mTabLayout;
    @BindView(R.id.viewPager)
    ViewPager mViewPager;
    private String mCityId;

    public static void create(Activity activity, int id) {
        Intent intent = new Intent(activity, DetailedWeatherActivity.class);
        intent.putExtra(CITY_ID, id);
        activity.startActivity(intent);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detailed_inform_activity);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        mCityId = String.valueOf(getIntent().getIntExtra(CITY_ID, 0));
        mViewPager.setAdapter(new WeatherPagerAdapter(getSupportFragmentManager(), this, mCityId));
        mTabLayout.setupWithViewPager(mViewPager);
    }
}
