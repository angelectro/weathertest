package angelectro.com.schoolandroid.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import angelectro.com.schoolandroid.R;
import angelectro.com.schoolandroid.content.find.ListCities;
import angelectro.com.schoolandroid.dialog.LoadingDialog;
import angelectro.com.schoolandroid.presenter.SearchPresenter;
import angelectro.com.schoolandroid.view.SearchCityView;
import angelectro.com.schoolandroid.widgets.SearchRecyclerAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zahit Talipov on 24.06.2016.
 */
public class FindCityActivity extends AppCompatActivity implements SearchCityView, SearchRecyclerAdapter.OnItemClickListener {

    public static final String CITY_ID = "city_id";
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.editTextSearch)
    EditText mEditTextSearch;
    @BindView(R.id.error_not_found)
    TextView mTextViewNotFound;
    SearchRecyclerAdapter mAdapter;
    SearchPresenter mPresenter;
    LoadingDialog mDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_city_activity);
        ButterKnife.bind(this);
        mAdapter = new SearchRecyclerAdapter(this);
        mPresenter = new SearchPresenter(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void showResult(List<ListCities> listCities) {
        mAdapter.update(listCities);
        mTextViewNotFound.setVisibility(View.GONE);
    }

    @Override
    public void showError(Throwable throwable) {
        mTextViewNotFound.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoading() {
        mDialog = new LoadingDialog();
        mDialog.show(getFragmentManager());
    }

    @Override
    public void hideLoading() {
        mDialog.dismiss();
    }

    public void search(View view) {
        mPresenter.find(mEditTextSearch.getText().toString());
    }

    @Override
    public void onClickItem(int id) {
        Intent intent = new Intent();
        intent.putExtra(CITY_ID, id);
        setResult(RESULT_OK, intent);
        finish();
    }
}
