package angelectro.com.schoolandroid.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import angelectro.com.schoolandroid.content.current.CurrentWeather;
import angelectro.com.schoolandroid.content.daily.DailyWeather;
import angelectro.com.schoolandroid.content.forecast.ForecastWeather;

/**
 * Created by Zahit Talipov on 23.06.2016.
 */
public class WeatherTable {

    public static void saveCurrent(@NonNull Context context, @NonNull CurrentWeather currentWeather) {
        save(context, String.valueOf(currentWeather.getId()), currentWeather, null, null);
    }

    public static void saveDaily(@NonNull Context context, @NonNull DailyWeather dailyWeather) {
        save(context, String.valueOf(dailyWeather.getCity().getId()), null, null, dailyWeather);
    }

    public static void saveForecast(@NonNull Context context, @NonNull ForecastWeather forecastWeather) {
        save(context, String.valueOf(forecastWeather.getCity().getId()), null, forecastWeather, null);
    }

    public static void delete(@NonNull Context context, @NonNull String id){
        Uri uri = Uri.withAppendedPath(SqLiteContentProvider.getWeatherUri(), id);
        context.getContentResolver().delete(uri,null,null);
    }

    public static List<CurrentWeather> listOfCitiesWeather(@NonNull Context context) {
        Cursor cursor = context.getContentResolver().query(SqLiteContentProvider.getWeatherUri(), null, null, null, null);
        List<CurrentWeather> currentWeathers = new ArrayList<>();
        Gson gson = new Gson();
        if (cursor != null ? cursor.moveToFirst() : false) {
            do {
                String json = new String(cursor.getBlob(cursor.getColumnIndex(DBContext.CURRENT_COLUMN)));
                CurrentWeather currentWeather = gson.fromJson(json, CurrentWeather.class);
                currentWeathers.add(currentWeather);
            }
            while (cursor.moveToNext());
        }
        return currentWeathers;
    }

    public static CurrentWeather getCurrentWeatherById(@NonNull Context context, String id) {
        Uri uri = Uri.withAppendedPath(SqLiteContentProvider.getWeatherUri(), id);
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        if (cursor.moveToFirst()) {
            Gson gson = new Gson();
            String json = new String(cursor.getBlob(cursor.getColumnIndex(DBContext.CURRENT_COLUMN)));
            CurrentWeather currentWeather = gson.fromJson(json, CurrentWeather.class);
            return currentWeather;
        }
        return null;
    }

    public static DailyWeather getDailyWeatherById(@NonNull Context context, String id) {
        Uri uri = Uri.withAppendedPath(SqLiteContentProvider.getWeatherUri(), id);
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        if (cursor.moveToFirst()) {
            Gson gson = new Gson();
            String json = new String(cursor.getBlob(cursor.getColumnIndex(DBContext.DAILY_COLUMN)));
            DailyWeather dailyWeather = gson.fromJson(json, DailyWeather.class);
            return dailyWeather;
        }
        return null;
    }

    public static ForecastWeather getForecastWeatherById(@NonNull Context context, String id) {
        Uri uri = Uri.withAppendedPath(SqLiteContentProvider.getWeatherUri(), id);
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        if (cursor.moveToFirst()) {
            Gson gson = new Gson();
            String json = new String(cursor.getBlob(cursor.getColumnIndex(DBContext.FORECAST_COLUMN)));
            ForecastWeather forecastWeather = gson.fromJson(json, ForecastWeather.class);
            return forecastWeather;
        }
        return null;
    }

    public static void save(@NonNull Context context, String id, CurrentWeather currentWeather, ForecastWeather forecastWeather, DailyWeather dailyWeather) {
        Uri uri = Uri.withAppendedPath(SqLiteContentProvider.getWeatherUri(), id);
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        byte[] current = new byte[0];
        byte[] daily = new byte[0];
        byte[] forecast = new byte[0];
        if (cursor.moveToFirst()) {
            current = cursor.getBlob(cursor.getColumnIndex(DBContext.CURRENT_COLUMN));
            daily = cursor.getBlob(cursor.getColumnIndex(DBContext.DAILY_COLUMN));
            forecast = cursor.getBlob(cursor.getColumnIndex(DBContext.FORECAST_COLUMN));
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBContext.ID_COLUMN, id);
        Gson gson = new Gson();
        if (currentWeather != null) {
            current = gson.toJson(currentWeather).getBytes();
        }
        if (dailyWeather != null) {
            daily = gson.toJson(dailyWeather).getBytes();
        }
        if (forecastWeather != null) {
            forecast = gson.toJson(forecastWeather).getBytes();
        }

        contentValues.put(DBContext.CURRENT_COLUMN, current);
        contentValues.put(DBContext.DAILY_COLUMN, daily);
        contentValues.put(DBContext.FORECAST_COLUMN, forecast);
        context.getContentResolver().insert(uri, contentValues);
    }
}
