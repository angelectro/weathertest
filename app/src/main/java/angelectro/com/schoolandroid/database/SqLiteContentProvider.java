package angelectro.com.schoolandroid.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

/**
 * Created by Zahit Talipov on 23.06.2016.
 */
public class SqLiteContentProvider extends ContentProvider {

    private static String sContentAuthority = "angelectro.com.schoolandroid";
    private static Uri sBaseUri = Uri.parse("content://" + sContentAuthority);
    private static Uri WEATHER_URI = Uri.parse("content://" + sContentAuthority + "/" + DBContext.WEATHER_TABLE_NAME);
    private static UriMatcher sUriMatcher;
    private static int URI_WEATHER = 1;

    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(sContentAuthority, DBContext.WEATHER_TABLE_NAME + "/#", URI_WEATHER);
    }

    public static Uri getWeatherUri() {
        return WEATHER_URI;
    }

    @NonNull
    public static String getContentAuthority() {
        return sContentAuthority;
    }

    @NonNull
    public static Uri getBaseUri() {
        return sBaseUri;
    }

    @Override
    public boolean onCreate() {
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] strings, String s, String[] strings1, String s1) {
        SQLiteDatabase database = new DatabaseHelper(getContext()).getWritableDatabase();
        if (sUriMatcher.match(uri) == 1) {
            String cityId = uri.getLastPathSegment();
            if (!TextUtils.isEmpty(cityId)) {
                s = DBContext.ID_COLUMN + " = " + cityId;
            }

            return database.query(DBContext.WEATHER_TABLE_NAME, strings, s, strings1, null, null, s1);
        } else {
            return database.query(DBContext.WEATHER_TABLE_NAME, strings, s, strings1, null, null, s1);
        }
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        if (sUriMatcher.match(uri) == 1) {
            SQLiteDatabase database = new DatabaseHelper(getContext()).getWritableDatabase();
            String cityId = uri.getLastPathSegment();
            long id = database.insertWithOnConflict(DBContext.WEATHER_TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
            return ContentUris.withAppendedId(WEATHER_URI, id);
        }
        return null;
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        if (sUriMatcher.match(uri) == 1) {
            SQLiteDatabase database = new DatabaseHelper(getContext()).getWritableDatabase();
            String cityId = uri.getLastPathSegment();
            if (!TextUtils.isEmpty(cityId)) {
                s = DBContext.ID_COLUMN + " = " + cityId;
            }

            return database.delete(DBContext.WEATHER_TABLE_NAME, s, strings);
        }
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;
    }
}
