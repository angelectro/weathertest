package angelectro.com.schoolandroid.database;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Zahit Talipov on 23.06.2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private static String DBName = "angelectro.com";
    private static int DBVersion = 1;
    public DatabaseHelper(Context context){
        super(context, DBName, null, DBVersion);
    }



    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DBContext.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
