package angelectro.com.schoolandroid.database;

/**
 * Created by Zahit Talipov on 23.06.2016.
 */
public class DBContext {
    public static String WEATHER_TABLE_NAME = "weather";
    public static String ID_COLUMN = "_ID";
    public static String CITY_NAME_COLUMN = "city";
    public static String CURRENT_COLUMN = "current";
    public static String FORECAST_COLUMN = "forecast";
    public static String DAILY_COLUMN = "daily";

    public static String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS "
            + WEATHER_TABLE_NAME + " ( "
            + ID_COLUMN + " INTEGER PRIMARY KEY,"
            + CITY_NAME_COLUMN + " STRING,"
            + CURRENT_COLUMN + " STRING,"
            + FORECAST_COLUMN + " STRING,"
            + DAILY_COLUMN + " STRING);";

}
