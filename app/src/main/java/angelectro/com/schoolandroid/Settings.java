package angelectro.com.schoolandroid;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by Zahit Talipov on 23.06.2016.
 */
public class Settings {

    private static final String NAME = "weather";
    private static final String CITIES_KEY = "cities";
    private static Context mContext;
    SharedPreferences mSharedPreferences;

    public static void init(Context mContext) {
        Settings.mContext = mContext;
    }

    public static void addCity(@NonNull String cityId) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        Set<String> listKeys = getSharedPreferences().getStringSet(CITIES_KEY, null);
        if (listKeys == null)
            listKeys = new HashSet<>();
        listKeys.add(cityId);
        editor.putStringSet(CITIES_KEY, listKeys);
        editor.commit();
        editor.apply();
    }

    public static void deleteCity(String id) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        Set<String> listKeys = getSharedPreferences().getStringSet(CITIES_KEY, null);
        Iterator<String> stringIterator = listKeys.iterator();
        String temp = null;
        while (stringIterator.hasNext()) {
            String s = stringIterator.next();
            if (s.contains(id))
                temp = s;
        }
        if (temp != null)
            listKeys.remove(temp);
        editor.putStringSet(CITIES_KEY, listKeys);
        editor.commit();
    }

    public static List<String> getListIdCities() {
        Set<String> listIdCities = getSharedPreferences().getStringSet(CITIES_KEY, new HashSet<String>());
        List<String> list = new ArrayList<>();
        list.addAll(listIdCities);
        return list;
    }

    private static SharedPreferences getSharedPreferences() {
        return mContext.getSharedPreferences(NAME, Context.MODE_PRIVATE);
    }
}
