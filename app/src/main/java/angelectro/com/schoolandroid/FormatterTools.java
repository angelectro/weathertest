package angelectro.com.schoolandroid;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import angelectro.com.schoolandroid.content.Day;
import angelectro.com.schoolandroid.content.forecast.ListDate;

/**
 * Created by Zahit Talipov on 24.06.2016.
 */
public class FormatterTools {
    public static String getDateString(int date) {
        Date d = new Date((long) date * 1000);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM", Locale.ENGLISH);
        return dateFormat.format(d);
    }

    public static String getHourString(String date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm");
        try {
            return hourFormat.format(simpleDateFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "--:--";
    }

    public static List<Day> convertDay(List<ListDate> listDates) {//2014-07-23 09:00:00
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM", Locale.ENGLISH);
        Date dateNow = new Date(System.currentTimeMillis());
        Day[] days = new Day[3];
        Log.d("time", simpleDateFormat.format(dateNow));
        try {
            int currentDay = Integer.parseInt(dayFormat.format(dateNow));
            for (ListDate date : listDates) {
                Date tempDate = simpleDateFormat.parse(date.getDt_txt());
                int day = Integer.parseInt(dayFormat.format(tempDate));
                int i = day - currentDay;
                if (i < 3 && i >= 0) {
                    if (days[i] == null) {
                        days[i] = new Day();
                        days[i].setDate(dateFormat.format(tempDate));
                    }
                    days[i].getListDates().add(date);
                }


            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Day> dayList = new ArrayList<>();
        for (Day day : days) {
            dayList.add(day);
        }
        return dayList;
    }
}
