
package angelectro.com.schoolandroid.content.current;


public class Wind {

    private Float speed;
    private Float deg;

    /**
     * 
     * @return
     *     The speed
     */
    public Float getSpeed() {
        return speed;
    }

    /**
     * 
     * @param speed
     *     The speed
     */
    public void setSpeed(Float speed) {
        this.speed = speed;
    }

    /**
     * 
     * @return
     *     The deg
     */
    public Float getDeg() {
        return deg;
    }

    /**
     * 
     * @param deg
     *     The deg
     */
    public void setDeg(Float deg) {
        this.deg = deg;
    }

}
