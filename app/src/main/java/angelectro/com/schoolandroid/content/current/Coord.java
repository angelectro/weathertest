
package angelectro.com.schoolandroid.content.current;


public class Coord {

    private Float lon;
    private Float lat;

    /**
     * 
     * @return
     *     The lon
     */
    public Float getLon() {
        return lon;
    }

    /**
     * 
     * @param lon
     *     The lon
     */
    public void setLon(Float lon) {
        this.lon = lon;
    }

    /**
     * 
     * @return
     *     The lat
     */
    public Float getLat() {
        return lat;
    }

    /**
     * 
     * @param lat
     *     The lat
     */
    public void setLat(Float lat) {
        this.lat = lat;
    }

}
