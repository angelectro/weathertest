package angelectro.com.schoolandroid.content;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import angelectro.com.schoolandroid.content.forecast.ListDate;

/**
 * Created by Zahit Talipov on 24.06.2016.
 */
public class Day {
    String mDate;
    List<ListDate> mListDates= new ArrayList<>();

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public List<ListDate> getListDates() {
        return mListDates;
    }

    public void setListDates(List<ListDate> listDates) {
        mListDates = listDates;
    }
}
