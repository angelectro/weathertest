
package angelectro.com.schoolandroid.content.current;


public class Rain {

    private Float _3h;

    /**
     * 
     * @return
     *     The _3h
     */
    public Float get3h() {
        return _3h;
    }

    /**
     * 
     * @param _3h
     *     The 3h
     */
    public void set3h(Float _3h) {
        this._3h = _3h;
    }

}
