
package angelectro.com.schoolandroid.content.forecast;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ForecastWeather {

    private City city;
    private String cod;
    private Double message;
    private Integer cnt;
    private java.util.List<ListDate> list = new ArrayList<>();

    /**
     * 
     * @return
     *     The city
     */
    public City getCity() {
        return city;
    }

    /**
     * 
     * @param city
     *     The city
     */
    public void setCity(City city) {
        this.city = city;
    }

    /**
     * 
     * @return
     *     The cod
     */
    public String getCod() {
        return cod;
    }

    /**
     * 
     * @param cod
     *     The cod
     */
    public void setCod(String cod) {
        this.cod = cod;
    }

    /**
     * 
     * @return
     *     The message
     */
    public Double getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The message
     */
    public void setMessage(Double message) {
        this.message = message;
    }

    /**
     * 
     * @return
     *     The cnt
     */
    public Integer getCnt() {
        return cnt;
    }

    /**
     * 
     * @param cnt
     *     The cnt
     */
    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }


    public java.util.List<ListDate> getList() {
        return list;
    }

    public void setList(java.util.List<ListDate> list) {
        this.list = list;
    }
}
